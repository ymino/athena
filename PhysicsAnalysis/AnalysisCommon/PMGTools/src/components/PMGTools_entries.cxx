#include "PMGTools/PMGSherpa22VJetsWeightTool.h"
#include "PMGTools/PMGSherpaVjetsSysTool.h"
#include "PMGTools/PMGCrossSectionTool.h"

using namespace PMGTools;

DECLARE_COMPONENT( PMGSherpa22VJetsWeightTool )
DECLARE_COMPONENT( PMGSherpaVjetsSysTool )
DECLARE_COMPONENT( PMGCrossSectionTool )

